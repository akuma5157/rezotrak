import { Component } from '@angular/core';
import { GoalService } from 'src/api';

@Component({
  selector: 'app-goal',
  template: `
    <button type="button" class="btn btn-primary" (click)="onSubmit()">
    Get Goal
    </button>
    <div *ngIf="message" [ngClass]="message.cssClass">{{message.text}}</div>
   `,
  styles: []
})
export class GoalComponent {
    message: any;
  constructor(private goalService: GoalService) { }

  onSubmit() {
    const message = {
      "text": null,
      "cssClass": null
    }
    console.log("getting server goal..");
    this.goalService.getAllGoals().subscribe(
      server_goal => {
        console.log("server goal: ", server_goal);
        message.cssClass = 'alert alert-dismissible alert-success';
        message.text = server_goal;
      },
      error => {
        console.log(error);
        message.cssClass = 'alert alert-dismissible alert-danger';
        message.text = error.error.detail;
      }
    );
    this.message = message;
  }
}
