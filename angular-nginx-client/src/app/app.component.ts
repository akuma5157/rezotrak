import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <app-goal></app-goal>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {
  title = 'rezotrak';
}
